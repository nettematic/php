FROM php:7.4-apache

RUN apt-get update && apt-get install -y apt-transport-https ca-certificates gnupg2
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    composer self-update --2
RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
	libwebp-dev \
    libpng-dev libxpm-dev \
    google-cloud-sdk \
    git \
    libicu-dev \
    libcurl4-openssl-dev \
    libzip-dev

RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql
RUN docker-php-ext-install json && docker-php-ext-enable json
RUN docker-php-ext-install intl && docker-php-ext-enable intl
RUN docker-php-ext-install pdo && docker-php-ext-enable pdo
RUN docker-php-ext-install curl && docker-php-ext-enable curl
RUN docker-php-ext-install bcmath && docker-php-ext-enable bcmath
RUN docker-php-ext-install zip && docker-php-ext-enable zip

RUN apt-get install -y libmemcached-dev libz-dev
RUN pecl install memcached-3.1.4
RUN docker-php-ext-enable memcached

RUN pecl install redis-5.1.1 \
    && pecl install xdebug-2.8.1 \
    && docker-php-ext-enable redis xdebug

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem -subj "/C=AT/ST=Vienna/L=Vienna/O=Security/OU=Development/CN=example.com"

WORKDIR /srv